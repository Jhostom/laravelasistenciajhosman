<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State_People extends Model
{
    protected $table='state_people';
	protected $fillable = ['person_id','state_id'];
    protected $guarded = ['id'];

    public function people() {
    	return $this->belongsToMany('App\Models\People');
    } 

    public function state() {
    	return $this->belongsToMany('App\Models\State');
    } 
}
