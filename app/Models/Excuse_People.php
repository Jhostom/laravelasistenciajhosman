<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Excuse_People extends Model
{
    protected $table='excuse_people';
	protected $fillable = ['excuse_id','person_id'];
    protected $guarded = ['id'];

    public function excuse() {
    	return $this->belongsToMany('App\Models\Excuse');
    } 

    public function people() {
    	return $this->belongsToMany('App\Models\People');
    } 
}
