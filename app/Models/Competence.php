<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Competence extends Model
{
    protected $table='competences';
	protected $fillable = ['competence_description','program_id','state_id'];
    protected $guarded = ['id'];

    public function program() {
    	return $this->belongsTo('App\Models\Program');
    } 

    public function state() {
    	return $this->belongsTo('App\Models\State');
    } 
}
