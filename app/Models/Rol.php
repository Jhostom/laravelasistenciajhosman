<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table='rols';
	protected $fillable = ['rol_name'];
    protected $guarded = ['id'];
}
