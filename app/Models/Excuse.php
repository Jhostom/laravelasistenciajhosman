<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Excuse extends Model
{
    protected $table='excuse';
	protected $fillable = ['excuse_date','excuse_picture','excuse_state','takeattendance_id'];
    protected $guarded = ['id'];

    public function takeAttendance() {
    	return $this->belongsTo('App\Models\Take_Attendance');
    } 
}
