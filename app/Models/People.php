<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    protected $table='people';
	protected $fillable = ['person_name','person_lastname','person_address','person_numberidentification','person_numberPhone','rol_id','user_id'];
    protected $guarded = ['id'];

    public function rol() {
    	return $this->belongsTo('App\Models\Rol');
    } 

    public function user() {
    	return $this->belongsTo('App\User');
    } 
}
