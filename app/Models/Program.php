<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
   	protected $table='programs';
	protected $fillable = ['program_name, state_id'];
    protected $guarded = ['id'];

    public function state() {
    	return $this->belongsTo('App\Models\State');
    } 
}
