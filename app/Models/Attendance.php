<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $table='attendance';
	protected $fillable = ['attendance_date','attendance_numberhours','groupnumber_id','competence_id'];
    protected $guarded = ['id'];

	public function groupNumber() {
    	return $this->belongsTo('App\Models\Group_Number');
    } 

    public function competence() {
    	return $this->belongsTo('App\Models\Competence');
    } 
}
