<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group_Number extends Model
{
    protected $table='group_numbers';
	protected $fillable = ['groupNumber_number','groupNumber_learningday','state_id','program_id'];
    protected $guarded = ['id'];

    public function state() {
    	return $this->belongsTo('App\Models\State');
    } 

    public function program() {
    	return $this->belongsTo('App\Models\Program');
    } 
}
