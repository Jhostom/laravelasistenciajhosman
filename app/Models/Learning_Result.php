<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Learning_Result extends Model
{
    protected $table='learning_results';
	protected $fillable = ['learningresults_description','competence_id'];
    protected $guarded = ['id'];

    public function competence() {
    	return $this->belongsTo('App\Models\Competence');
    } 
}
