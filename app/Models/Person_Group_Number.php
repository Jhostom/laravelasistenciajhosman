<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person_Group_Number extends Model
{
    protected $table='person_group_number';
	protected $fillable = ['person_id','groupnumber_id'];
    protected $guarded = ['id'];

    public function people() {
    	return $this->belongsToMany('App\Models\People');
    } 

    public function groupNumber() {
    	return $this->belongsToMany('App\Models\Group_Number');
    } 
}
