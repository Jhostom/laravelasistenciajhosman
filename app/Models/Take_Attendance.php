<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Take_Attendance extends Model
{
    protected $table='take_attendance';
	protected $fillable = ['takeattendance_state','attendance_id','person_id'];
    protected $guarded = ['id'];

    public function people() {
    	return $this->belongsToMany('App\Models\People');
    } 

    public function attendance() {
    	return $this->belongsToMany('App\Models\Attendance');
    } 
}
