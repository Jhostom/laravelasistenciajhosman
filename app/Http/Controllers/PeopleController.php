<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\People as People;
use App\Models\Rol as Rol;
use App\User as User;
use Illuminate\Support\Facades\Hash;

class PeopleController extends Controller 
{   
    public function destroy($id)
    {
        $people = People::find($id);
        $people->delete();
        return redirect()->back();
    }
    public function edit($id)
    {
        $people = People::find($id);
        $rol = Rol::all();
        return \View::make('people/update',compact('people','rol'));
    }
    public function update($id, Request $request)
    {
        $people = People::find($id);
        $people->person_name = $request->name;
        $people->person_lastname = $request->lastname;
        $people->person_address = $request->address;
        $people->person_numberidentification= $request->identification;
        $people->person_numberPhone = $request->phone;
        $people->rol_id= $request->rol;
        $people->save();
        return redirect('people');
    }
    public function index()
    {
        $people = People::all();
        return \View::make('people/list',compact('people'));
    }
    public function create()
    {
        $rol = Rol::all();
    	return \View::make('people/new', compact('people','rol'));
    }
    public function store(Request $request)
    {   
        $user = new User;
        $user->name=$request->name2;
        $user->email=$request->email;
        $user->password=Hash::make($request->password);
        $user->state_id=1;
        $user->rol=$request->rol;
        $user->save();
        $users = User::all()->max('id');
        $a = $users;
        //Instanciacion de clase people e invocacion de atributos para registro de persona
    	$people = new People;
    	$people->person_name = $request->name2;
    	$people->person_lastname = $request->lastname;
    	$people->person_address = $request->address;
    	$people->person_numberidentification = $request->identification;
    	$people->person_numberPhone = $request->phone;
    	$people->rol_id = $request->rol;
        $people->user_id = $a;
        $people->save();
        return redirect('people');
    }
}
