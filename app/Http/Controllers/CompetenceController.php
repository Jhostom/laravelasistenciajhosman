<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Competence as Competence;
use App\Models\Program as Program;
use App\Models\State as State;

class CompetenceController extends Controller
{
	public function destroy($id)
    {
        $competence = Competence::find($id);
        $competence->delete();
        return redirect()->back();
    }
    public function edit($id)
    {
    	$state = State::all();
        $competence = Competence::find($id);
        $program = Program::all();
        return \View::make('competence/update',compact('competence','program','state'));
    }
    public function update($id, Request $request)
    {
        $competence = Competence::find($id);
        $competence->competence_description = $request->description;
        $competence->program_id= $request->program;
        $competence->state_id= $request->state;
        $competence->save();
        return redirect('competence');
    }
    public function index()
    {
        $competence = Competence::all();
        $program = Program::all();
        return \View::make('competence/list',compact('competence','program'));
    }
    public function create()
    {
        $program = Program::all();
    	return \View::make('competence/new', compact('competence','program'));
    }
    public function store(Request $request)
    {   
        //Instanciacion de clase Competence e invocacion de atributos para registro de Competencia
    	$competence = new Competence;
    	$competence->competence_description = $request->description;
    	$competence->program_id= $request->program;
    	$competence->state_id = 1;
        $competence->save();
        return redirect('competence');
    }
}
