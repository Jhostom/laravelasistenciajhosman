<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rol as Rol;

class RoleController extends Controller
{
	public function destroy($id)
    {
        $rol = Rol::find($id);
        $rol->delete();
        return redirect()->back();
    }
    public function edit($id)
    {
    	$rol = Rol::find($id);        
        return \View::make('rol/update',compact('rol'));
    }
    public function update($id, Request $request)
    {
        $rol = Rol::find($id);
        $rol->rol_name = $request->name;
        $rol->save();
        return redirect('rol');
    }
    public function index()
    {
        $rol = Rol::all();        
        return \View::make('rol/list', compact('rol'));
    }
    public function create()
    {        
    	return \View::make('rol/new', compact('rol'));
    }
    public function store(Request $request)
    {           
    	$rol = new Rol;
    	$rol->rol_name = $request->name;
        $rol->save();
        return redirect('rol');
    }
}
