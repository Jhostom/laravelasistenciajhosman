<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\State as State;

class StateController extends Controller
{
	public function destroy($id)
    {
        $state = State::find($id);
        $state->delete();
        return redirect()->back();
    }
    public function edit($id)
    {
    	$state = State::find($id);        
        return \View::make('state/update',compact('state'));
    }
    public function update($id, Request $request)
    {
        $state = State::find($id);
        $state->state = $request->name;
        $state->save();
        return redirect('state');
    }
    public function index()
    {
        $state = State::all();        
        return \View::make('state/list', compact('state'));
    }
    public function create()
    {        
    	return \View::make('state/new', compact('state'));
    }
    public function store(Request $request)
    {           
    	$state = new State;
    	$state->state = $request->name;
        $state->save();
        return redirect('state');
    }
}
