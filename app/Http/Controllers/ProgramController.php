<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Program as Program;
use App\Models\State as State;

class ProgramController extends Controller
{
	public function destroy($id)
    {
        $program = Program::find($id);
        $program->delete();
        return redirect()->back();
    }
    public function edit($id)
    {
    	$state = State::all();        
        $program = Program::find($id);
        return \View::make('program/update',compact('program','state'));
    }
    public function update($id, Request $request)
    {
        $program = Program::find($id);
        $program->program_name = $request->name;
        $program->state_id= $request->state;
        $program->save();
        return redirect('program');
    }
    public function index()
    {
        $program = Program::all();        
        $state = State::all();
        return \View::make('program/list', compact('program','state'));
    }
    public function create()
    {        
        $state = State::all();        
    	return \View::make('program/new', compact('program'));
    }
    public function store(Request $request)
    {           
    	$program = new Program;
    	$program->program_name = $request->name;            	
    	$program->state_id = 1;
        $program->save();
        return redirect('program');
    }
}
