<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Learning_Result as Learning_Result;
use App\Models\Competence as Competence;

class LearningResultController extends Controller
{
	public function destroy($id)
    {
        $learningResult = learning_Result::find($id);
        $learningResult->delete();
        return redirect()->back();
    }
    public function edit($id)
    {
        $learningResult = learning_Result::find($id);
        $competence = Competence::all();        
        return \View::make('learningResults/update',compact('learningResult','competence'));
    }
    public function update($id, Request $request)
    {
        $learningResult = learning_Result::find($id);
        $learningResult->learningResult_description = $request->description;
        $learningResult->competence_id = $request->competence;        
        $learningResult->save();
        return redirect('learningResult');
    }
    public function index()
    {
        $learningResult = learning_Result::all();
        $competence = Competence::all();
        return \View::make('learningResults/list',compact('learningResult','competence'));
    }
    public function create()
    {
        $competence = Competence::all();
    	return \View::make('learningResults/new', compact('learningResult','competence'));
    }
    public function store(Request $request)
    {           
    	$learningResult = new Learning_Result;
    	$learningResult->learningResult_description = $request->description;
    	$learningResult->competence_id= $request->competence;    	
        $learningResult->save();
        return redirect('learningResult');
    }
}
