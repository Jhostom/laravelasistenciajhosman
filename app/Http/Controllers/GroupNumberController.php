<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Group_Number as Group_Number;
use App\Models\Program as Program;
use App\Models\State as State;

class GroupNumberController extends Controller
{
	public function destroy($id)
    {
        $groupNumber = Group_Number::find($id);
        $groupNumber->delete();
        return redirect()->back();
    }
    public function edit($id)
    {
    	$state = State::all();
        $groupNumber = Group_Number::find($id);
        $program = Program::all();
        return \View::make('groupNumber/update',compact('groupNumber','program','state'));
    }
    public function update($id, Request $request)
    {
        $groupNumber = Group_Number::find($id);
        $groupNumber->groupNumber_number = $request->number;
        $groupNumber->groupNumber_learningday = $request->learningday;
        $groupNumber->program_id= $request->program;
        $groupNumber->state_id= $request->state;
        $groupNumber->save();
        return redirect('groupNumber');
    }
    public function index()
    {
        $groupNumber = Group_Number::all();
        $program = Program::all();
        $state = State::all();
        return \View::make('groupNumber/list', compact('groupNumber','program','state'));
    }
    public function create()
    {
        $program = Program::all();
        $state = State::all();        
    	return \View::make('groupNumber/new', compact('groupNumber','competence','program'));
    }
    public function store(Request $request)
    {           
    	$groupNumber = new Group_Number;
    	$groupNumber->groupNumber_number = $request->number;
        $groupNumber->groupNumber_learningday = $request->learningday;
    	$groupNumber->program_id= $request->program;
    	$groupNumber->state_id = 1;
        $groupNumber->save();
        return redirect('groupNumber');
    }
}
