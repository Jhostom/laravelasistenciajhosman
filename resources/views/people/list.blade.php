@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-12">
				{!!Form::open(['route' => 'people/show','method' => 'post','novalidate', 'class' => 'form-inline'])!!}
				<div class="form-group">
					<label>Nombre</label>
					<input type="text" name="name" class="form-control">	
				</div>
				<div class="form-group">
					<!-- <button type="submit" class="btn btn-default">Buscar</button> -->
					<a href="{{route('people.index')}}" class="btn btn-primary">Ver Todo</a>
					<a href="{{route('people.create')}}" class="btn btn-primary">Crear</a>
				</div>
				{!! Form::close() !!}
			</article>
			<article class="col-md-12">
				<table class="table table-info table-condensed table-striped table-bordered ">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Direccion</th>
							<th>Identificacion</th>
							<th>Telefono</th>
							<th>Rol</th>
							<th>Usuario</th>
						</tr>
					</thead>
					<tbody>
						@foreach($people as $people1)
							<tr>
								<td>{{ $people1->person_name}}</td>
								<td>{{ $people1->person_lastname}}</td>
								<td>{{ $people1->person_address}}</td>
								<td>{{ $people1->person_numberidentification}}</td>
								<td>{{ $people1->person_numberphone}}</td>
								<td>{{ $people1->rol->rol_name}}</td>
								<td>{{ $people1->user->name}}</td>								
								<td>
									<a href="{{ route('people.edit',['id' => $people1->id]) }}" class="btn btn-info">Editar</a>
									<a href="{{ route('people/destroy',['id' => $people1->id]) }}" class="btn btn-danger">Eliminar</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</article>
		</div>
	</section>
@endsection