@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!! Form::model($people,['route' => ['people.update',$people->id],'method' => 'put','novalidate']) !!}
					<div class="form-group">
						<label>Nombre</label>
						<input type="text" name="name" class="form-control" required value="{{ $people->person_name}}">	
					</div>
					<div class="form-group">
						<label>Apellido</label>
						<input type="text" name="lastname" class="form-control" required value="{{ $people->person_lastname}}">	
					</div>
					<div class="form-group">
						<label>Direccion</label>
						<input type="text" name="address" class="form-control" required value="{{ $people->person_address}}">	
					</div>
					<div class="form-group">
						<label>Identificacion</label>
						<input type="number" name="identification" class="form-control" required value="{{ $people->person_numberidentification}}">	
					</div>
					<div class="form-group">
						<label>Telefono</label>
						<input type="text" name="phone" class="form-control" required value="{{ $people->person_numberphone}}">	
					</div>
					<div class="from-group">
						<label>Seleccione el Rol</label>
						<select name="rol">	
						@foreach($rol as $rol)
							<option value="{{$rol->id}}">{{$rol->rol_name}}</option>
						@endforeach
						</select>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-success">Enviar</button>	
					</div>
				{!! Form::close()!!}
			</article>
		</div>
	</section>
@endsection
