@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-12">
				{!!Form::open(['route' => 'rol/show','method' => 'post','novalidate', 'class' => 'form-inline'])!!}
				<div class="form-group">
					<label>Nombre</label>
					<input type="text" name="name" class="form-control">	
				</div>
				<div class="form-group">
					<!-- <button type="submit" class="btn btn-default">Buscar</button> -->
					<a href="{{route('rol.index')}}" class="btn btn-primary">Ver Todo</a>
					<a href="{{route('rol.create')}}" class="btn btn-success">Crear</a>
				</div>
				{!! Form::close() !!}
			</article>
			<article class="col-md-12">
				<table class="table table-info table-condensed table-striped table-bordered ">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Acción</th>
						</tr>
					</thead>
					<tbody>
						@foreach($rol as $rol1)
							<tr>
								<td>{{ $rol1->rol_name}}</td>								
								<td>
									<a href="{{ route('rol.edit',['id' => $rol1->id]) }}" class="btn btn-info">Editar</a>
									<a href="{{ route('rol/destroy',['id' => $rol1->id]) }}" class="btn btn-danger">Eliminar</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</article>
		</div>
	</section>
@endsection