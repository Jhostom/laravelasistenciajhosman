@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!! Form::model($competence,['route' => ['competence.update',$competence->id],'method' => 'put','novalidate']) !!}
				<div class="form-group row">
                    <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Descripcion de la competencia') }}</label>

                    <div class="col-md-6">
                        <input id="description" type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ $competence->competence_description}}" required autofocus>

                        @if ($errors->has('description'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

				<div class="form-group row">
                    <label for="program" class="col-md-4 col-form-label text-md-right">{{ __('Seleccione el Programa') }}</label>

                    <div class="col-md-6">
                        <select id="program" class="form-control" name="program" required autofocus>
                            @foreach($program as $program1)
                                <option value="{{$program1->id}}">{{$program1->program_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
				

				<div class="form-group row">
                    <label for="state" class="col-md-4 col-form-label text-md-right">{{ __('Seleccione el Estado') }}</label>

                    <div class="col-md-6">
                        <select id="state" class="form-control" name="state" required autofocus>
                            @foreach($state as $state1)
                                <option value="{{$state1->id}}">{{$state1->state}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
				
    			<div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Register') }}
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
			</article>
		</div>
	</section>
@endsection
