@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-12">
				{!!Form::open(['route' => 'competence/show','method' => 'post','novalidate', 'class' => 'form-inline'])!!}
				<div class="form-group">
					<label>Nombre</label>
					<input type="text" name="name" class="form-control">	
				</div>
				<div class="form-group">
					<!-- <button type="submit" class="btn btn-default">Buscar</button> -->
					<a href="{{route('competence.index')}}" class="btn btn-primary">Ver Todo</a>
					<a href="{{route('competence.create')}}" class="btn btn-primary">Crear</a>
				</div>
				{!! Form::close() !!}
			</article>
			<article class="col-md-12">
				<table class="table table-info table-condensed table-striped table-bordered ">
					<thead>
						<tr>
							<th>Descripcion</th>
							<th>Programa</th>
							<th>Estado</th>
						</tr>
					</thead>
					<tbody>
						@foreach($competence as $competence1)
							<tr>
								<td>{{ $competence1->competence_description}}</td>
								<td>{{ $competence1->program->program_name}}</td>
								<td>{{ $competence1->state->state}}</td>
								<td>
									<a href="{{ route('competence.edit',['id' => $competence1->id]) }}" class="btn btn-info">Editar</a>
									<a href="{{ route('competence/destroy',['id' => $competence1->id]) }}" class="btn btn-danger">Eliminar</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</article>
		</div>
	</section>
@endsection