@extends('layouts.app')
@section('content')
    <section class="container">
        <div class="row">
            <article class="col-md-10 col-md-offset-1">
				{!!Form::open(['route' => 'groupNumber.store','method' => 'post','novalidate'])!!}
                <div class="form-group row">
                    <label for="number" class="col-md-4 col-form-label text-md-right">{{ __('Número De La Dicha De Caracterización') }}</label>
                    <div class="col-md-6">
                        <input id="number" type="number" class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" name="number" value="{{ old('number') }}" required autofocus>

                        @if ($errors->has('number'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('number') }}</strong>
                            </span>
                        @endif
                    </div>                                                          
                </div>
                <div class="form-group row">
                    <label for="learningday" class="col-md-4 col-form-label text-md-right">{{ __('Jornada De La Dicha De Caracterización') }}</label>
                    <div class="col-md-6">
                        <input id="learningday" type="text" class="form-control{{ $errors->has('learningday') ? ' is-invalid' : '' }}" name="learningday" value="{{ old('learningday') }}" required autofocus>

                        @if ($errors->has('learningday'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('learningday') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>
				<div class="form-group row">
                    <label for="program" class="col-md-4 col-form-label text-md-right">{{ __('Seleccione el programa') }}</label>
                    <div class="col-md-6">
                        <select id="program" class="form-control" name="program" required autofocus>
                            @foreach($program as $program1)
                                <option value="{{$program1->id}}">{{$program1->program_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
				
    			<div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Registrar') }}
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </article>
		</div>
	</section>
@endsection