@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!! Form::model($groupNumber,['route' => ['groupNumber.update',$groupNumber->id],'method' => 'put','novalidate']) !!}
				<div class="form-group row">
                    <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Número de la ficha de caracterización') }}</label>
                    <div class="col-md-6">
                        <input id="number" type="number" class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" name="number" value="{{ $groupNumber->groupnumber_number}}" required autofocus>

                        @if ($errors->has('number'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('number') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="learningday" class="col-md-4 col-form-label text-md-right">{{ __('Jornada de la ficha de caracterización') }}</label>
                    <div class="col-md-6">
                        <input id="learningday" type="text" class="form-control{{ $errors->has('learningday') ? ' is-invalid' : '' }}" name="learningday" value="{{ $groupNumber->groupnumber_learningday}}" required autofocus>

                        @if ($errors->has('learningday'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('learningday') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
				<div class="form-group row">
                    <label for="program" class="col-md-4 col-form-label text-md-right">{{ __('Seleccione el programa') }}</label>
                    <div class="col-md-6">
                         <select id="program" class="form-control" name="program" required autofocus>
                            @foreach($program as $program1)
                                <option value="{{$program1->id}}">{{$program1->program_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>	
                <div class="form-group row">
                    <label for="state" class="col-md-4 col-form-label text-md-right">{{ __('Seleccione el estado') }}</label>
                    <div class="col-md-6">
                         <select id="state" class="form-control" name="state" required autofocus>
                            @foreach($state as $state1)
                                <option value="{{$state1->id}}">{{$state1->state}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>  			
    			<div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Actualizar') }}
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
			</article>
		</div>
	</section>
@endsection
