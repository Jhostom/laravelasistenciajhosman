@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-12">
				{!!Form::open(['route' => 'groupNumber/show','method' => 'post','novalidate', 'class' => 'form-inline'])!!}
				<div class="form-group">
					<label>Número De Ficha: </label>
					<input type="text" name="number" class="form-control">	
				</div>
				<div class="form-group">
					<!-- <button type="submit" class="btn btn-default">Buscar</button> -->
					<a href="{{route('groupNumber.index')}}" class="btn btn-primary">Ver Todo</a>
					<a href="{{route('groupNumber.create')}}" class="btn btn-primary">Crear</a>
				</div>
				{!! Form::close() !!}
			</article>
			<article class="col-md-12">
				<table class="table table-info table-condensed table-striped table-bordered ">
					<thead>
						<tr>
							<th>Número</th>
							<th>Jornada</th>							
							<th>Programa</th>							
							<th>Estado</th>	
							<th>Acción</th>							
						</tr>
					</thead>
					<tbody>
						@foreach($groupNumber as $groupNumber1)
							<tr>								
								<td>{{ $groupNumber1->groupnumber_number }}</td>
								<td>{{ $groupNumber1->groupnumber_learningday }}</td>
								<td>{{ $groupNumber1->program->program_name }}</td>								
								<td>{{ $groupNumber1->state->state }}</td>								
								<td>
									<a href="{{ route('groupNumber.edit',['id' => $groupNumber1->id]) }}" class="btn btn-info">Editar</a>
									<a href="{{ route('groupNumber/destroy',['id' => $groupNumber1->id]) }}" class="btn btn-danger">Eliminar</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</article>
		</div>
	</section>
@endsection