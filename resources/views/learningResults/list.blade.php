@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-12">
				{!!Form::open(['route' => 'learningResult/show','method' => 'post','novalidate', 'class' => 'form-inline'])!!}
				<div class="form-group">
					<label>Descripción</label>
					<input type="text" name="description" class="form-control">	
				</div>
				<div class="form-group">
					<!-- <button type="submit" class="btn btn-default">Buscar</button> -->
					<a href="{{route('learningResult.index')}}" class="btn btn-primary">Ver Todo</a>
					<a href="{{route('learningResult.create')}}" class="btn btn-primary">Crear</a>
				</div>
				{!! Form::close() !!}
			</article>
			<article class="col-md-12">
				<table class="table table-info table-condensed table-striped table-bordered ">
					<thead>
						<tr>
							<th>Descripción</th>
							<th>Competencia</th>							
						</tr>
					</thead>
					<tbody>
						@foreach($learningResult as $learningResult1)
							<tr>								
								<td>{{ $learningResult1->learningresult_description}}</td>
								<td>{{ $learningResult1->competence->competence_description}}</td>								
								<td>
									<a href="{{ route('learningResult.edit',['id' => $learningResult1->id]) }}" class="btn btn-info">Editar</a>
									<a href="{{ route('learningResult/destroy',['id' => $learningResult1->id]) }}" class="btn btn-danger">Eliminar</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</article>
		</div>
	</section>
@endsection