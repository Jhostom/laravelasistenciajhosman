<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Asistencia</title>
        <link rel="icon" href="{{ asset('images/Sena.ico') }}">
        <!-- Fonts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!-- <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> -->
        <!-- Styles -->
        <style>
            body{
                background-image: url(../images/fondo.png)!important;
                -webkit-background-size: cover !important;
                -moz-background-size: cover !important;
                -o-background-size: cover !important;
                background-size: cover !important;
                background-position: center !important;
                background-repeat:no-repeat !important;
                background-attachment: fixed !important;
            }
            #titulo{
                font-size: 56px;
                color: 
            }
            #paragraph{
                font-size: 25px;
                font-weight: normal;
            }
            #booton{
                font-size: 30px;
                padding-left: 30px;
                padding-right: 30px;
                border-top-left-radius:58px;
                border-top-right-radius:58px;
                border-bottom-left-radius:58px;
                border-bottom-right-radius:58px;
                background-color: rgba(0, 142, 0, 1);
            }
            .carousel-inner{
                max-height: 100vh;
            }
            .carousel-caption {
                padding: 15px;
                position: fixed;
                background-attachment: fixed;
                border-top-left-radius:58px;
                border-top-right-radius:58px;
                border-bottom-left-radius:58px;
                border-bottom-right-radius:58px;
                background-color: rgba(0, 0, 0, 0.2);
                margin-bottom: 18%;
                z-index: 10;
            }
            .carousel-caption2 {
                padding: 15px;
                position: fixed;
                background-attachment: fixed;
                width: 100%;
                background-color: rgba(0, 0, 0, 0.2);
                margin-top: -54px;
                z-index: 10;
            }
            
            /*.full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }*/
        </style>
    </head>
    <body>
        <div id="Carrousel" class="carousel slide carousel-fade" data-ride="carousel">
          <div class="carousel-inner">
          <div class="carousel-caption text-Center shadow-lg">
            <h1 id="titulo">¡Bienvenido!</h1>
            <p id="paragraph">Este es el aplicativo en el que podrás desarrollar los procesos correspondientes al llamado de Asistencia de los aprendices del área de Articulación con la media, específicamente del centro de gestión de mercados, logística y tecnologías de la información</p>
            @auth
                    <p><a id="booton" class="btn btn-lg btn-success" href="{{ url('/home') }}" role="button">  Inicio  </a></p>
                @else
                    <p><a id="booton" class="btn btn-lg btn-success" href="{{ route('login') }}" role="button">Ingresar</a></p>
                @endauth
          </div>
            <div class="carousel-item active" data-duration="200">
              <img class="d-block w-100" src="{{asset('images/aprendiz-sena.jpg')}}" alt="First slide">
            </div>
            <div class="carousel-item " data-duration="200">
              <img class="d-block w-100" src="{{asset('images/aprendiz-sena2.jpg')}}" alt="Second slide">
              <div class="container">
            </div>
            </div>
            <div class="carousel-item" data-duration="200">
              <img class="d-block w-100" src="{{asset('images/aprendices.png')}}" alt="Third slide">
            </div>
          </div>
        </div>
        <div class= "carousel-caption2 text-center text-info">
            <footer >Imagenes Sujetas a Derechos de Autor. Tomadas de: http://senasofiapluscursos.info, http://static.hsbnoticias.com, http://static.hsbnoticias.com</footer>
            </div>
        <!-- <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div> -->
    </body>
</html>
