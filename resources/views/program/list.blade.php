@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-12">
				{!!Form::open(['route' => 'program/show','method' => 'post','novalidate', 'class' => 'form-inline'])!!}
				<div class="form-group">
					<label>Nombre del programa: </label>
					<input type="text" name="name" class="form-control">	
				</div>
				<div class="form-group">
					<!-- <button type="submit" class="btn btn-default">Buscar</button> -->
					<a href="{{route('program.index')}}" class="btn btn-primary">Ver Todo</a>
					<a href="{{route('program.create')}}" class="btn btn-success">Crear</a>
				</div>
				{!! Form::close() !!}
			</article>
			<article class="col-md-12">
				<table class="table table-info table-condensed table-striped table-bordered ">
					<thead>
						<tr>
							<th>Nombre</th>							
							<th>Estado</th>	
							<th>Acción</th>							
						</tr>
					</thead>
					<tbody>
						@foreach($program as $program1)
							<tr>								
								<td>{{ $program1->program_name }}</td>								
								<td>{{ $program1->state->state }}</td>								
								<td>
									<a href="{{ route('program.edit',['id' => $program1->id]) }}" class="btn btn-info">Editar</a>
									<a href="{{ route('program/destroy',['id' => $program1->id]) }}" class="btn btn-danger">Eliminar</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</article>
		</div>
	</section>
@endsection