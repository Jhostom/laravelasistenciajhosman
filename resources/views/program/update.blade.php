@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!! Form::model($program,['route' => ['program.update',$program->id],'method' => 'put','novalidate']) !!}
				<div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre Del Programa De Formación') }}</label>
                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $program->program_name}}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>                
                <div class="form-group row">
                    <label for="state" class="col-md-4 col-form-label text-md-right">{{ __('Seleccione el estado') }}</label>
                    <div class="col-md-6">
                         <select id="state" class="form-control" name="state" required autofocus>
                            @foreach($state as $state1)
                                <option value="{{$state1->id}}">{{$state1->state}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>  			
    			<div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Actualizar') }}
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
			</article>
		</div>
	</section>
@endsection
