<?php

use Illuminate\Database\Seeder;

use App\Models\Rol;

class RolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$rols = array(
                    [
                        'rol_name' => 'Instructor'
                    ],
                    [
                        'rol_name' => 'Aprendiz'
                    ],
                    [
                        'rol_name' => 'Bienestar'
                    ],
                    [
                        'rol_name' => 'Instructor Lider'
                    ],
                    [
                        'rol_name' => 'Administrativo'
                    ]
                );
        
        foreach ($rols as $value) {
            $rols = new Rol;
            $rols->rol_name = $value['rol_name'];
            $rols->save();
        }
    }
}
