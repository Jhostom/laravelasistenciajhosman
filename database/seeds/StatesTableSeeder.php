<?php

use Illuminate\Database\Seeder;
//class para crear estados
use App\Models\State;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$states = array(
                    [
                        'state' => 'Activo'
                    ],
                    [
                        'state' => 'Inactivo'
                    ]
                );
        
        foreach ($states as $value) {
            $state = new State;
            $state->state = $value['state'];
            $state->save();
        }
    }
}
