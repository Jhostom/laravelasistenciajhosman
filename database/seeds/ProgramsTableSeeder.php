<?php

use Illuminate\Database\Seeder;
//class para crear programas
use App\Models\Program;

class ProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$programs = array(
                    [
                        'program_name' => 'ADSI'
                    ],
                    [
                        'program_name' => 'MANTENIMIENTO'
                    ],
                    [
                        'program_name' => 'LOGÍSTICA'
                    ]
                );
        
        foreach ($programs as $value) {
            $program = new Program;
            $program->program_name = $value['program_name'];
            $program->state_id = 1;
            $program->save();
        }
    }
}
