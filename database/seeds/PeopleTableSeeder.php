<?php

use Illuminate\Database\Seeder;
//Class faker para generar datos aleatorios
use Faker\Factory as Faker;
//Class people para crear nuevos Usuarios.
use App\Models\People as People;

class PeopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i=0; $i <1 ; $i++) { 
	        $people = new People;
	    	$people->person_name = $faker->name;
	    	$people->person_lastname = $faker->lastname;
	    	$people->person_address = 'calle123';
	    	$people->person_numberidentification = 1234567890;
	    	$people->person_numberPhone = 3044933084;
	    	$people->rol_id = 5;
	        $people->user_id = 1;
	        $people->save();
        }
    }
}
