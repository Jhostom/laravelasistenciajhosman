<?php

use Illuminate\Database\Seeder;
//Class faker. Componente generador de datos aleatorios.
use Faker\Factory as Faker;
//Class user para crear nuevos Usuarios.
use App\User;
//Uso de Facade Has para generar passwords encriptados.
use Illuminate\Support\Facades\Hash;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i <1 ; $i++) { 
        	$user = new User;
        	$user->name = $faker->name;
        	$user->email = $faker->email;
        	$user->password = Hash::make('test123');
        	$user->state_id = 1;
            $user->rol = 5;
        	$user->save();
        }
    }
}
