<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->foreign('state_id')->references('id')->on('states')->onUpdate('cascade');
        });
        Schema::table('competences', function($table)
        {
            $table->foreign('program_id')->references('id')->on('programs')->onUpdate('cascade');
            $table->foreign('state_id')->references('id')->on('states')->onUpdate('cascade');
        });
        Schema::table('excuse_people', function($table)
        {
            $table->foreign('excuse_id')->references('id')->on('excuses')->onUpdate('cascade');
            $table->foreign('person_id')->references('id')->on('people')->onUpdate('cascade');
        });
        Schema::table('take_attendances', function($table)
        {
            $table->foreign('attendance_id')->references('id')->on('attendances')->onUpdate('cascade');
            $table->foreign('person_id')->references('id')->on('people')->onUpdate('cascade');
        });
        Schema::table('excuses', function($table)
        {
            $table->foreign('takeattendance_id')->references('id')->on('take_attendances')->onUpdate('cascade');
        });
        Schema::table('group_numbers', function($table)
        {
            $table->foreign('state_id')->references('id')->on('states')->onUpdate('cascade');
            $table->foreign('program_id')->references('id')->on('programs')->onUpdate('cascade');
        });
        Schema::table('learning_results', function($table)
        {
            $table->foreign('competence_id')->references('id')->on('competences')->onUpdate('cascade');
        });
        Schema::table('people', function($table)
        {
            $table->foreign('rol_id')->references('id')->on('rols')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade');
        });
        Schema::table('person_group_numbers', function($table)
        {
            $table->foreign('person_id')->references('id')->on('people')->onUpdate('cascade');
            $table->foreign('groupnumber_id')->references('id')->on('group_numbers')->onUpdate('cascade');
        });
        Schema::table('programs', function($table)
        {
            $table->foreign('state_id')->references('id')->on('states')->onUpdate('cascade');
        });
        Schema::table('state_people', function($table)
        {
            $table->foreign('person_id')->references('id')->on('people')->onUpdate('cascade');
            $table->foreign('state_id')->references('id')->on('states')->onUpdate('cascade');
        });
        Schema::table('attendances', function($table)
        {
            $table->foreign('groupnumber_id')->references('id')->on('group_numbers')->onUpdate('cascade');
            $table->foreign('competence_id')->references('id')->on('competences')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
