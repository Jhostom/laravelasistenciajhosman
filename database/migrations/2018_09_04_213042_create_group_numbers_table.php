<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_numbers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('groupnumber_number');
            $table->string('groupnumber_learningday');
            $table->integer('state_id')->unsigned();
            $table->integer('program_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_numbers');
    }
}
