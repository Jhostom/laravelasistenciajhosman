<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::resource('people','PeopleController');
Route::get('people/destroy/{id}', ['as' => 'people/destroy', 'uses' => 'PeopleController@destroy']);
Route::post('people/show', ['as' => 'people/show', 'uses' => 'PeopleController@show']);

Route::resource('competence','competenceController');
Route::get('competence/destroy/{id}', ['as' => 'competence/destroy', 'uses' => 'competenceController@destroy']);
Route::post('competence/show', ['as' => 'competence/show', 'uses' => 'competenceController@show']);

Route::resource('learningResult','learningResultController');
Route::get('learningResult/destroy/{id}', ['as' => 'learningResult/destroy', 'uses' => 'learningResultController@destroy']);
Route::post('learningResult/show', ['as' => 'learningResult/show', 'uses' => 'learningResultController@show']);

Route::resource('groupNumber','GroupNumberController');
Route::get('groupNumber/destroy/{id}', ['as' => 'groupNumber/destroy', 'uses' => 'GroupNumberController@destroy']);
Route::post('groupNumber/show', ['as' => 'groupNumber/show', 'uses' => 'GroupNumberController@show']);

Route::resource('program','ProgramController');
Route::get('program/destroy/{id}', ['as' => 'program/destroy', 'uses' => 'ProgramController@destroy']);
Route::post('program/show', ['as' => 'program/show', 'uses' => 'ProgramController@show']);

Route::resource('rol','RoleController');
Route::get('rol/destroy/{id}', ['as' => 'rol/destroy', 'uses' => 'RoleController@destroy']);
Route::post('rol/show', ['as' => 'rol/show', 'uses' => 'RoleController@show']);

Route::resource('state','StateController');
Route::get('state/destroy/{id}', ['as' => 'state/destroy', 'uses' => 'StateController@destroy']);
Route::post('state/show', ['as' => 'state/show', 'uses' => 'StateController@show']);


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
